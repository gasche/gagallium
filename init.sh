# get the article sources
git clone inria:/home/yquem/cristal/scherer/gagallium/gagallium-source

# get a copy of the backup repository
git clone inria:/home/yquem/cristal/scherer/gagallium/gagallium-save
# You can also create a new backup repository if you are starting a new blog:
#   git init gagallium-save

# create the directory for test renderings
mkdir gagallium-test

# create the directory for final renderings
mkdir gagallium-output
